<?php
$conexion = new mysqli("localhost", "root", "", "nba");
if ($conexion->connect_errno) {
    echo "¡Ooooh noooo! Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
} else {
$resultado = $conexion->query("SELECT * FROM jugadores");
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta charset="utf-8">
    <title>Jugadores</title>
  </head>
  <body>
    <table align=center class="striped">
        <thead>
          <tr style="background-color: rgb(255,205,225)">
            <th style="text-align:center;padding:25px">Codigo</th>
            <th style="text-align:center;padding:25px">Nombre</th>
            <th style="text-align:center;padding:25px">Procedencia</th>
            <th style="text-align:center;padding:25px">Altura</th>
            <th style="text-align:center;padding:25px">Peso</th>
            <th style="text-align:center;padding:25px">Posición</th>
            <th style="text-align:center;padding:25px">Nombre del equipo</th>
          </tr>
        </thead>
        <tbody>
      <?php foreach ($resultado as $jugadores) {
        echo "<tr>";
        echo "<td style='text-align:center'>".$jugadores["codigo"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Nombre"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Procedencia"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Altura"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Peso"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Posicion"]."</td>";
        echo "<td style='text-align:center'>".$jugadores["Nombre_equipo"]."</td>";
      }
      ?>
      </tbody>
      </table>
  </body>
</html>
