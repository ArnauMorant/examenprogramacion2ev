<?php
$conexion = new mysqli("localhost", "root", "", "nba");
if ($conexion->connect_errno) {
    echo "¡Atención cuidado esto no es un simulacro! Fallo al conectar a MySQL: (".$conexion->connect_errno.")".$conexion->connect_error;
} else {
$resulta2 = $conexion->query("SELECT * FROM equipos");
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta charset="utf-8">
    <title>Equipo</title>
  </head>
  <body>
    <table align=center class="striped">
        <thead>
          <tr style="background-color: rgb(255,205,225)">
            <th style="text-align:center;padding:25px">Nombre</th>
            <th style="text-align:center;padding:25px">Ciudad</th>
            <th style="text-align:center;padding:25px">Conferencia</th>
            <th style="text-align:center;padding:25px">División</th>
          </tr>
        </thead>
        <tbody>
      <?php foreach ($resulta2 as $equipo) {
        echo "<tr>";
        echo "<td style='text-align:center'>"."<a href='jugadores.php?equipo=".$equipo["Nombre"]."'>".$equipo["Nombre"]."</a></td>";
        echo "<td style='text-align:center'>".$equipo["Ciudad"]."</td>";
        echo "<td style='text-align:center'>".$equipo["Conferencia"]."</td>";
        echo "<td style='text-align:center'>".$equipo["Division"]."</td>";
      }
      ?>
      </tbody>
      </table>
  </body>
</html>
