<?php
$conexion = new mysqli("localhost", "root", "", "nba");
if ($conexion->connect_errno) {
    echo "¡Atención cuidado! Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
} else {
$resultado = $conexion->query("SELECT * FROM partidos");
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta charset="utf-8">
    <title>Index</title>
  </head>
  <body>
    <table align=center class="striped">
        <thead>
          <tr style="background-color: rgb(225,255,205)">
            <th style="text-align:center;padding:25px">Código</th>
            <th style="text-align:center;padding:25px">Equipo local</th>
            <th style="text-align:center;padding:25px">Equipo visitante</th>
            <th style="text-align:center;padding:25px">Puntos local</th>
            <th style="text-align:center;padding:25px">Puntos visitante</th>
            <th style="text-align:center;padding:25px">Temporada</th>
          </tr>
        </thead>
        <tbody>
      <?php foreach ($resultado as $partidos) {
        echo "<tr>";
        echo "<td style='text-align:center'>".$partidos["codigo"]."</td>";
        echo "<td style='text-align:center'>"."<a href='equipo.php?nombre=".$partidos['equipo_local']."'>".$partidos["equipo_local"]."</a></td>";
        echo "<td style='text-align:center'>"."<a href='equipo.php?nombre=".$partidos['equipo_visitante']."'>".$partidos["equipo_visitante"]."</td>";
        echo "<td style='text-align:center'>".$partidos["puntos_local"]."</td>";
        echo "<td style='text-align:center'>".$partidos["puntos_visitante"]."</td>";
        echo "<td style='text-align:center'>".$partidos["temporada"]."</td>";
      }
      ?>
      </tbody>
      </table>
  </body>
</html>
